// Lecture: Hoisting

calculateAge(1990); // 28

function calculateAge(year) {
    console.log(2019-year);
}

retirement(1990); // This will not work

var retirement = function(year) {
    console.log(65-(2019-year));
}

console.log(age); // undefined
var age = 30;

function foo() {
    var age = 65;  
    console.log(age); // function context: 65
}
foo();
console.log(age); // global context: 30


// Lecture: Scoping
// JS only creates scopes for functions, not flow control blocks. JS also has lexical scoping, inner functions can access outer function scope. 
// Show the difference between execution stack and scope chain
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        third()
    }
} // 'Hey!Hi!Hello!'

function third() {
    var d = 'John';
    console.log(a + b + c + d);
} // Uncaught ReferenceError


// Lecture: The this keyword
// In function calls, 'this' points at the global object ('Window' object). In method calls, 'this' points to the object calling the method. 'this' is not assigned a value until a function where it is defined is called. 
console.log(this); // Returns the 'Window' object

/*
calculateAge(1985);

function calculateAge(year) {
    console.log(2019-year); // 33
    console.log(this); // 'Window' object
}
*/

var john = {
    name: 'John',
    yearOfBirth: 1990,
    calculateAge: function() {
        console.log(this); // 'john' object
        console.log(2019-this.yearOfBirth) // 28
        
        /*
        function innerFunction() {
            console.log(this); // 'Window' object
        }
        
        innerFunction();
        */
    }
}

john.calculateAge();

var mike = {
    name: 'Mike',
    yearOfBirth: 1984
}

// Method borrowing
mike.calculateAge = john.calculateAge;
mike.calculateAge(); // 'mike' object; 34